package com.example.jobana.repositories

import com.example.jobana.entities.OfferEntity
import com.example.jobana.entities.ProfileEntity
import com.example.jobana.entities.UserEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface OfferRepository : CrudRepository<OfferEntity, UUID>{
    fun findByProfileIdAndUserId(profileId : UUID, userId: UUID) : OfferEntity?

    fun findAllByProfileUser(user : UserEntity) : List<OfferEntity>

    fun findAllByProfile(profile : ProfileEntity) : List<OfferEntity>
}