package com.example.jobana.models.response

import com.example.jobana.entities.*
import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDateTime
import java.util.*

data class AdvertResponse(
    val id : UUID,
    val authorId: UUID,
    val title: String,
    val shortDescription: String,
    val description: String,
    @get:JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    val createdAt : LocalDateTime,
    val attachments : List<UUID>,
    val price: Int,
    val city: String?,
    val categories : List<String>,
    val isClosed : Boolean
){
    companion object{
        fun fromEntity(entity : AdvertEntity) =
            AdvertResponse(
                id = entity.id,
                authorId = entity.author.id,
                title = entity.title,
                shortDescription = entity.shortDescription,
                description = entity.description,
                createdAt = entity.createdAt,
                attachments = entity.attachments.map { it.id },
                price = entity.price,
                city = entity.city?.name,
                categories = entity.categories.map { it.name },
                isClosed = entity.closedAt != null
            )
    }
}