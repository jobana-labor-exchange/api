package com.example.jobana.repositories

import com.example.jobana.entities.CityEntity
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.util.UUID

interface CityRepository : CrudRepository<CityEntity, UUID> {
    fun getByName(name : String) : CityEntity?

    @Query("SELECT * FROM cities;", nativeQuery = true)
    fun getAll() : List<CityEntity>
}