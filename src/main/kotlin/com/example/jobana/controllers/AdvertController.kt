package com.example.jobana.controllers

import com.example.jobana.models.request.AdvertRequest
import com.example.jobana.entities.ReplyEntity
import com.example.jobana.models.request.AdvertChangeStatus
import com.example.jobana.models.response.AdvertResponse
import com.example.jobana.models.response.ApiResponse
import com.example.jobana.services.AdvertService
import com.example.jobana.services.AttachmentService
import com.example.jobana.services.ReplyService
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.util.*


@RestController
@RequestMapping("/adverts")
class AdvertController(
    val advertService: AdvertService,
    val replyService: ReplyService,
    val attachmentService: AttachmentService
) {

    @GetMapping("/my")
    fun getUserAdverts(): ResponseEntity<List<AdvertResponse>> {
        return ResponseEntity(advertService.getUserAdverts(), HttpStatus.OK)
    }

    @GetMapping
    fun searchPaged(
        @RequestParam(defaultValue = "0") page: Int,
        @RequestParam(defaultValue = "10") size: Int,
        @RequestParam(required = false) city : String?,
        @RequestParam(required = false) category : List<String>?
    ): ResponseEntity<Page<AdvertResponse>> {
        return ResponseEntity(advertService.searchPaged(page, size, city, category), HttpStatus.OK)
    }

    @GetMapping("/{id}")
    fun getAdvertById(@PathVariable id: UUID): ResponseEntity<AdvertResponse> {
        return ResponseEntity(AdvertResponse.fromEntity(advertService.getAdvertById(id)), HttpStatus.OK)
    }

    @GetMapping("/{id}/replies")
    fun getProfileReplies(@PathVariable id: UUID) : ResponseEntity<List<ReplyEntity>>{
        return ResponseEntity(replyService.getAdvertReplies(id), HttpStatus.OK)
    }

    @PostMapping("/{id}/replies")
    fun addReply(@PathVariable id: UUID) : ResponseEntity<ReplyEntity>{
        return ResponseEntity(replyService.addReply(id), HttpStatus.OK)
    }

    @DeleteMapping("/{id}/replies")
    fun cancelReply(@PathVariable id: UUID) : ResponseEntity<ApiResponse>{
        return ResponseEntity(replyService.cancelReply(id), HttpStatus.OK)
    }

    @PostMapping
    fun createAdvert(@RequestBody advertRequest: AdvertRequest): ResponseEntity<AdvertResponse> {
        val newAdvert = advertService.createAdvert(advertRequest)
        return ResponseEntity(AdvertResponse.fromEntity(newAdvert), HttpStatus.CREATED)
    }

    @PutMapping("/{id}")
    fun editAdvert(@RequestBody advertRequest: AdvertRequest, @PathVariable id: UUID): ResponseEntity<AdvertResponse> {
        return ResponseEntity(advertService.editAdvert(advertRequest, id), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    fun deleteAdvert(@PathVariable id: UUID): ResponseEntity<ApiResponse> {
        return advertService.deleteAdvert(id).asResponse()
    }

    @PatchMapping("/{id}")
    fun changeAdvertStatus(
        @PathVariable id: UUID,
        @RequestBody advertChangeStatus: AdvertChangeStatus
    ): ResponseEntity<ApiResponse> {
        return advertService.changeAdvertStatus(advertChangeStatus, id).asResponse()
    }

    @PostMapping("/{id}/attachments")
    fun uploadAttachments(@PathVariable id : UUID, @RequestParam("files") files : List<MultipartFile>): ResponseEntity<ApiResponse> {
        return attachmentService.uploadAttachments(id, files).asResponse()
    }

}