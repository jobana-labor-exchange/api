package com.example.jobana.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*
import jakarta.validation.constraints.Pattern
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.time.LocalDate
import java.time.LocalDateTime


@Entity
@Table(name = "users")
class UserEntity(
    @Column(unique = true, nullable = false, length = 320)
    @Pattern(regexp = "^[\\w-.]+@([\\w-]+.)+[\\w-]+$")
    var email: String,

    @JsonIgnore
    @Column(name = "password", nullable = false, length = 256)
    var passwd: String,

    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    var role: Role,

    @Column(nullable = false, length = 32)
    var firstName: String,

    @Column(nullable = false, length = 32)
    var lastName: String,

    @Column(nullable = false)
    var birthDate: LocalDate,

    @Column(nullable = false, length = 5)
    @Enumerated(EnumType.STRING)
    var gender: Gender,


    @JsonIgnore
    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "photo_id", referencedColumnName = "id", nullable = true)
    var profilePhoto: PhotoEntity?,


    @Column
    var lastLogIn: LocalDateTime,


    @JsonIgnore
    @OneToMany(mappedBy = "author")
    // adverts posted by this user
    var adverts: List<AdvertEntity>,


    @JsonIgnore
    @OneToMany(mappedBy = "user")
    // replies made by this user
    var replies: List<ReplyEntity>,

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    // replies made by this user
    var profiles: List<ProfileEntity>,

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    var contacts: List<ContactEntity>,


    @JsonIgnore
    @OneToMany(mappedBy = "user")
    // offers made by this user
    var offers: List<OfferEntity>


) : GenericUUIDEntity(), UserDetails {
    // implementing UserDetails
    override fun getAuthorities() = listOf(SimpleGrantedAuthority(role.name))
    override fun getPassword(): String = passwd
    override fun getUsername(): String = email
    override fun isAccountNonExpired() = true
    override fun isAccountNonLocked() = true
    override fun isCredentialsNonExpired() = true
    override fun isEnabled(): Boolean = true
}


