package com.example.jobana.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*

@Entity
@Table(name="photos")
class PhotoEntity(

    @JsonIgnore
    @OneToOne(mappedBy = "profilePhoto", cascade = [CascadeType.ALL])
    var user: UserEntity?,


    @Column(nullable = false)
    var filePath : String,

    @Column(nullable = false)
    var contentType : String
) : GenericUUIDEntity()