package com.example.jobana.controllers

import com.example.jobana.entities.OfferEntity
import com.example.jobana.models.request.ProfileRequest
import com.example.jobana.models.response.ApiResponse
import com.example.jobana.models.response.ProfileResponse
import com.example.jobana.services.OfferService
import com.example.jobana.services.ProfilesService
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController
@RequestMapping("/profiles")
class ProfileController(
    val profilesService: ProfilesService,
    val offerService: OfferService
) {

    @GetMapping("/my")
    fun getUserProfiles(): ResponseEntity<List<ProfileResponse>> {
        return ResponseEntity(profilesService.getUserProfiles(), HttpStatus.OK)
    }

    @GetMapping
    fun searchPaged(
        @RequestParam(defaultValue = "0") page: Int,
        @RequestParam(defaultValue = "10") size: Int,
        @RequestParam(required = false) city: String?,
        @RequestParam(required = false) category: List<String>?
    ): ResponseEntity<Page<ProfileResponse>> {
        return ResponseEntity(profilesService.searchPaged(page, size, city, category), HttpStatus.OK)
    }

    @GetMapping("/{id}")
    fun getProfileById(@PathVariable id: UUID): ResponseEntity<ProfileResponse> {
        return ResponseEntity(ProfileResponse.fromEntity(profilesService.getProfileById(id)), HttpStatus.OK)
    }

    @GetMapping("/{id}/offers")
    fun getProfileOffers(@PathVariable id: UUID) : ResponseEntity<List<OfferEntity>>{
        return ResponseEntity(offerService.getProfileOffers(id), HttpStatus.OK)
    }

    @PostMapping("/{id}/offers")
    fun addOffer(@PathVariable id: UUID) : ResponseEntity<OfferEntity>{
        return ResponseEntity(offerService.addOffer(id), HttpStatus.OK)
    }

    @DeleteMapping("/{id}/offers")
    fun cancelOffer(@PathVariable id: UUID) : ResponseEntity<ApiResponse>{
        return ResponseEntity(offerService.cancelOffer(id), HttpStatus.OK)
    }

    @PostMapping
    fun createProfile(@RequestBody profileRequest: ProfileRequest): ResponseEntity<ProfileResponse> {
        val newProfile = profilesService.createProfile(profileRequest)
        return ResponseEntity(newProfile, HttpStatus.CREATED)
    }

    @PutMapping("/{id}")
    fun editProfile(
        @RequestBody profileRequest: ProfileRequest,
        @PathVariable id: UUID
    ): ResponseEntity<ProfileResponse> {
        return ResponseEntity(profilesService.editProfile(profileRequest, id), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    fun deleteProfile(@PathVariable id: UUID): ResponseEntity<ApiResponse> {
        return profilesService.deleteProfile(id).asResponse()
    }


}