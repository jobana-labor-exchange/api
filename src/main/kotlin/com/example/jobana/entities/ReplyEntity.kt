package com.example.jobana.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.Entity
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "replies")
class ReplyEntity(
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "advert_id", nullable = false)
    var advert: AdvertEntity,

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    var user: UserEntity

) : GenericUUIDEntity()