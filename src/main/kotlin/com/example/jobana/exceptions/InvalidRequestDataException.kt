package com.example.jobana.exceptions

import org.springframework.http.HttpStatus

class InvalidRequestDataException (
    status: HttpStatus = HttpStatus.UNPROCESSABLE_ENTITY,
    message : String = "Unable to process the request"
) : AbstractApiException(status, message)