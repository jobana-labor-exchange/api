package com.example.jobana.repositories

import com.example.jobana.entities.ContactEntity
import com.example.jobana.entities.UserEntity
import org.springframework.data.repository.CrudRepository
import java.util.UUID

interface ContactsRepository : CrudRepository<ContactEntity, UUID>{
    fun getAllByUser(user : UserEntity) : List<ContactEntity>
}