package com.example.jobana.repositories

import com.example.jobana.entities.*
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.UUID

interface ProfilesRepository : CrudRepository<ProfileEntity, UUID>, PagingAndSortingRepository<ProfileEntity, UUID>{
    fun findAllByUser(user : UserEntity) : List<ProfileEntity>

    @Query("""SELECT DISTINCT p.id, p.created_at, p.cv, p.short_description, p.title,
            p.category_id, p.user_id, p.city_id FROM profiles p
            LEFT JOIN cities ci ON p.city_id = ci.id
            WHERE ci.name = :city OR ci.name IS NULL 
            ORDER BY p.created_at DESC""", nativeQuery = true)
    fun findAllByCityPaged(city : String, pageable: Pageable) : Page<ProfileEntity>

    @Query("""SELECT DISTINCT p.id, p.created_at, p.cv, p.short_description, p.title,
            p.category_id, p.user_id, p.city_id FROM profiles p
            INNER JOIN adverts_categories ac on p.id = ac.adverts_id
            INNER JOIN categories c ON ac.categories_id = c.id
            LEFT JOIN cities ci ON p.city_id = ci.id
            WHERE c.name IN :categories AND (ci.name = :city OR ci.name IS NULL ) 
            ORDER BY p.created_at DESC""", nativeQuery = true)
    fun findAllByCategoriesAndCityPaged(categories: List<String>, city: String, pageable: Pageable) : Page<ProfileEntity>

    @Query("""SELECT DISTINCT p.id, p.created_at, p.cv, p.short_description, p.title,
            p.category_id, p.user_id, p.city_id FROM profiles p
            INNER JOIN adverts_categories ac on p.id = ac.adverts_id
            INNER JOIN categories c ON ac.categories_id = c.id
            WHERE c.name IN :categories
            ORDER BY p.created_at DESC""", nativeQuery = true)
    fun findAllByCategoriesPaged(categories: List<String>, pageable: Pageable) : Page<ProfileEntity>

}