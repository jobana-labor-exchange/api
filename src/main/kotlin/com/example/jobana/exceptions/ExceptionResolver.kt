package com.example.jobana.exceptions

import com.example.jobana.models.response.ApiResponse
import org.slf4j.LoggerFactory

import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest

@ControllerAdvice
@Component
class ExceptionResolver {
    private val logger = LoggerFactory.getLogger(ExceptionResolver::class.java)

    @ExceptionHandler(value = [InvalidRequestDataException::class])
    protected fun handleInvalidRequest(cause: InvalidRequestDataException, request: WebRequest): ResponseEntity<ApiResponse> {
        logger.info(cause.stackTraceToString())
        return cause.asResponse()
    }

    @ExceptionHandler(value = [ResourceNotFoundException::class])
    protected fun handleMotFound(cause: ResourceNotFoundException, request: WebRequest): ResponseEntity<ApiResponse> {
        logger.info(cause.stackTraceToString())
        return cause.asResponse()
    }

    @ExceptionHandler(value = [BadRequestException::class])
    protected fun handleBadRequest(cause: BadRequestException, request: WebRequest): ResponseEntity<ApiResponse> {
        logger.info(cause.stackTraceToString())
        return cause.asResponse()
    }

    @ExceptionHandler(value = [ForbiddenException::class])
    protected fun handleForbidden(cause: ForbiddenException, request: WebRequest): ResponseEntity<ApiResponse> {
        logger.info(cause.stackTraceToString())
        return cause.asResponse()
    }

    @ExceptionHandler(value = [Throwable::class])
    protected fun handleThrowable(cause: Throwable, request: WebRequest): ResponseEntity<ApiResponse> {
        logger.error(cause.stackTraceToString())
        return InternalServerException().asResponse()
    }

}
