package com.example.jobana.models.response

import org.springframework.http.HttpStatus
import java.time.LocalDateTime

class DeletedResponse(
    override val message: String = "Successfully deleted"
) : ApiResponse{
    override val status = HttpStatus.OK
    override val timestamp: LocalDateTime = LocalDateTime.now()

}
