package com.example.jobana.entities

enum class Gender {
    M, F
}

enum class Role {
    USER
}