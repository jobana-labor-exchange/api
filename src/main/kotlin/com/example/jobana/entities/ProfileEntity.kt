package com.example.jobana.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*


@Entity
@Table(name="profiles")
class ProfileEntity(
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    var user: UserEntity,

    @Column(nullable = false, length = 64)
    var title: String,

    @Column(length = 128)
    var shortDescription: String,


    @Column(nullable = false, columnDefinition = "TEXT")
    var cv: String,

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "city_id")
    var city: CityEntity?,

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
    var category: CategoryEntity,

    ) : GenericUUIDEntity()