package com.example.jobana.services

import com.example.jobana.entities.AttachmentEntity
import com.example.jobana.entities.UserEntity
import com.example.jobana.exceptions.BadRequestException
import com.example.jobana.exceptions.ForbiddenException
import com.example.jobana.exceptions.InternalServerException
import com.example.jobana.exceptions.ResourceNotFoundException
import com.example.jobana.models.response.ApiResponse
import com.example.jobana.models.response.DeletedResponse
import com.example.jobana.repositories.AdvertRepository
import com.example.jobana.repositories.AttachmentRepository
import org.springframework.core.io.ByteArrayResource
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.time.LocalDateTime
import java.util.*

@Service
class AttachmentService(
    val attachmentRepository: AttachmentRepository,
    val advertRepository: AdvertRepository
) {

    fun downloadAttachment(id: UUID): ResponseEntity<ByteArray> {
        // create dir with photos if not exists
        Files.createDirectories(Paths.get("./attachments"))

        val attachment = attachmentRepository.findByIdOrNull(id)
            ?: throw ResourceNotFoundException(message = "Attachment with id $id does not exist")

        val filePath = attachment.filePath

        val file = File(filePath)
        if (!file.exists()) {
            throw ResourceNotFoundException(message = "File with path '$filePath' does not exist")
        }

        val resource = ByteArrayResource(Files.readAllBytes(Paths.get(file.absolutePath)))

        return ResponseEntity.ok()
            .headers(HttpHeaders())
            .contentLength(file.length())
            .contentType(MediaType.parseMediaType(attachment.contentType))
            .body(resource.byteArray)


    }

    fun deleteAttachment(id: UUID): DeletedResponse {

        // create dir with photos if not exists
        Files.createDirectories(Paths.get("./attachments"))

        val attachment = attachmentRepository.findByIdOrNull(id)
            ?: throw ResourceNotFoundException(message = "Attachment with id $id does not exist")

        val filePath = attachment.filePath

        val file = File(filePath)
        if (!file.exists()) {
            throw ResourceNotFoundException(message = "File with path '$filePath' does not exist")
        }

        file.delete()
        attachmentRepository.delete(attachment)

        return DeletedResponse(message = "Attachment with id $id deleted successfully")
    }

    fun uploadAttachments(id: UUID, files: List<MultipartFile>): ApiResponse {
        // create dir with photos if not exists
        Files.createDirectories(Paths.get("./attachments"))

        for (file in files) {
            // check if file has content type
            if (file.contentType == null) {
                throw BadRequestException(message = "Multipart file content type was unknown")
            }


            // check if file has valid content type
            //if (!file.contentType!!.startsWith("image/")) {
            //    throw BadRequestException(message = "Multipart file should have image/* content type")
            //}



            val fileContentType = file.contentType!!

            // check if user can edit this advert
            val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity
            val advert = advertRepository.findByIdOrNull(id)
                ?: throw ResourceNotFoundException(message = "Advert with id $id does not exist")
            if(advert.author.id != currentUser.id){
                throw ForbiddenException(message = "Current user has no edit access to advert $id")
            }


            // new file path
            val newFilePath = "./attachments/${UUID.randomUUID()}"
            val newFile = File(newFilePath)

            // name collision
            if (newFile.exists()) {
                throw InternalServerException(message = "File '$newFilePath' already exists")
            }

            // create file with new profile photo
            newFile.createNewFile()

            // write image bytes to file
            Files.write(Path.of(newFilePath), file.bytes)

            // create new photo entity or reuse the old one
            val newAttachmentEntity = AttachmentEntity(newFilePath, fileContentType, advert)

            // save changes to db
            attachmentRepository.save(newAttachmentEntity)
        }

        return object : ApiResponse {
            override val message = "Saved advert attachments (${files.size}) successfully"
            override val status = HttpStatus.OK
            override val timestamp = LocalDateTime.now()
        }

    }
}