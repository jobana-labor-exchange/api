package com.example.jobana.controllers

import com.example.jobana.models.response.HealthResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@Tag(name = "Статус работы")
class HealthController {
    @GetMapping("/health")
    fun healthCheck() = HealthResponse().asResponse()

}

