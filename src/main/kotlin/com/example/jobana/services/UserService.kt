package com.example.jobana.services

import com.example.jobana.exceptions.InternalServerException
import com.example.jobana.security.JwtService
import com.example.jobana.exceptions.InvalidRequestDataException
import com.example.jobana.models.request.LoginRequest
import com.example.jobana.models.request.RegisterRequest
import com.example.jobana.models.response.ApiResponse
import com.example.jobana.models.response.AuthResponse
import com.example.jobana.entities.Gender
import com.example.jobana.entities.PhotoEntity
import com.example.jobana.entities.Role
import com.example.jobana.entities.UserEntity
import com.example.jobana.exceptions.BadRequestException
import com.example.jobana.exceptions.ResourceNotFoundException
import com.example.jobana.models.request.PatchUserRequest
import com.example.jobana.models.response.UserDataResponse
import com.example.jobana.repositories.PhotoRepository
import com.example.jobana.repositories.UserRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.time.LocalDateTime
import java.util.UUID


@Service
class UserService(
    val userRepository: UserRepository,
    val passwordEncoder: PasswordEncoder,
    val jwtService: JwtService,
    val authenticationManager: AuthenticationManager,
    val photoRepository: PhotoRepository
) {

    fun getUserData(): UserDataResponse {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity
        return UserDataResponse.fromEntity(currentUser)
    }

    fun editUserData(patchUserRequest: PatchUserRequest): UserDataResponse {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        // retrieve gender
        val gender = patchUserRequest.gender.let {
            try {
                Gender.valueOf(it)
            } catch (e: IllegalArgumentException) {
                throw InvalidRequestDataException(message = "Invalid field value : gender")
            }
        }

        // make changes
        currentUser.firstName = patchUserRequest.firstName
        currentUser.lastName = patchUserRequest.lastName
        currentUser.birthDate = patchUserRequest.birthDate
        currentUser.gender = gender

        // save changes
        userRepository.save(currentUser)

        return UserDataResponse.fromEntity(currentUser)

    }

    fun registerUser(registerRequest: RegisterRequest): AuthResponse {

        // check email format
        val emailRegex = Regex("^[\\w-.]+@([\\w-]+.)+[\\w-]+$")
        if (!registerRequest.email.matches(emailRegex)) {
            throw InvalidRequestDataException(message = "Invalid field value : email")
        }

        // retrieve gender
        val gender = registerRequest.gender.let {
            try {
                Gender.valueOf(it)
            } catch (e: IllegalArgumentException) {
                throw InvalidRequestDataException(message = "Invalid field value : gender")
            }
        }

        if (userRepository.findByEmail(registerRequest.email) != null) {
            throw InvalidRequestDataException(message = "User with '${registerRequest.email}' email already exists")
        }

        val userEntity = UserEntity(
            registerRequest.email,
            passwordEncoder.encode(registerRequest.password),
            Role.USER,
            registerRequest.firstName,
            registerRequest.lastName,
            registerRequest.birthDate,
            gender,
            null,
            LocalDateTime.now(),
            listOf(), listOf(), listOf(), listOf(), listOf()
        )

        // generate new token for this user
        val jwt = jwtService.generateToken(userEntity)

        userRepository.save(userEntity)

        return AuthResponse(jwt, HttpStatus.OK, "Signed up successfully")
    }

    fun authenticateUser(loginRequest: LoginRequest): AuthResponse {
        // check if user with this email exists
        val userEntity = userRepository.findByEmail(loginRequest.email)
            ?: return AuthResponse(
                null, HttpStatus.UNAUTHORIZED,
                "User with email '${loginRequest.email}' not found"
            )

        authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(
                loginRequest.email,
                loginRequest.password
            )
        )
        // generate new token for this user
        val jwt = jwtService.generateToken(userEntity)

        // save current login time
        userEntity.lastLogIn = LocalDateTime.now()
        userRepository.save(userEntity)

        return AuthResponse(jwt, HttpStatus.OK, "Successful authentication")

    }

    fun saveProfilePicture(multipartFile: MultipartFile): ApiResponse {

        // check if file has content type
        if (multipartFile.contentType == null) {
            throw BadRequestException(message = "Multipart file content type was unknown")
        }
        // check if file has valid content type
        if (!multipartFile.contentType!!.startsWith("image/")) {
            throw BadRequestException(message = "Multipart file should have image/* content type")
        }

        val fileContentType = multipartFile.contentType!!

        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        // create dir with photos if not exists
        Files.createDirectories(Paths.get("./profile-photos"))

        // delete old photo file if exists
        val oldProfilePhoto = currentUser.profilePhoto
        oldProfilePhoto?.let {
            val file = File(it.filePath)
            if (file.exists()) {
                file.delete()
            }
        }

        // new photo path
        val photoPath = "./profile-photos/${UUID.randomUUID()}"
        val photoFile = File(photoPath)

        // name collision
        if (photoFile.exists()) {
            throw InternalServerException(message = "File '$photoPath' already exists")
        }

        // create file with new profile photo
        photoFile.createNewFile()

        // write image bytes to file
        Files.write(Path.of(photoPath), multipartFile.bytes)

        // create new photo entity or reuse the old one
        val newProfilePhoto =
            oldProfilePhoto?.also { it.filePath = photoPath;it.contentType = fileContentType }
                ?: PhotoEntity(currentUser, photoPath, fileContentType)

        // save changes to db
        photoRepository.save(newProfilePhoto)
        currentUser.profilePhoto = newProfilePhoto
        userRepository.save(currentUser)


        return object : ApiResponse {
            override val message = "Saved profile picture successfully"
            override val status = HttpStatus.OK
            override val timestamp = LocalDateTime.now()
        }
    }

    fun getProfilePicture(): ResponseEntity<ByteArray> {

        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        val defaultPhotoFile = File("./profile-photos/default-photo.png")

        val (photoBytes, contentType) = if (currentUser.profilePhoto == null) {
            // user has no profile picture, return default one
            Pair(defaultPhotoFile.readBytes(), "image/png")
        } else {
            // check if profile photo row is present in db
            val photoEntity = photoRepository.findByIdOrNull(currentUser.profilePhoto!!.id)
                ?: throw ResourceNotFoundException(message = "Profile photo was not found in db")

            val photoFile = File(photoEntity.filePath)
            // check if photo file exists on disk
            if (!photoFile.exists()) {
                throw ResourceNotFoundException(message = "Profile photo was not found on disk")
            }
            Pair(photoFile.readBytes(), photoEntity.contentType)
        }


        return ResponseEntity.ok()
            .contentType(MediaType.valueOf(contentType))
            .body(photoBytes)
    }
}