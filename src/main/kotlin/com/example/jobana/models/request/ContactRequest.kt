package com.example.jobana.models.request

data class ContactRequest(
    val key: String,
    val value: String,
)