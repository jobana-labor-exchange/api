package com.example.jobana.repositories

import com.example.jobana.entities.UserEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface UserRepository : CrudRepository<UserEntity, UUID>{
    fun findByEmail(email : String) : UserEntity?
}
