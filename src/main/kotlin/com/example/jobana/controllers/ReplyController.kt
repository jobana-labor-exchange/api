package com.example.jobana.controllers

import com.example.jobana.entities.ReplyEntity
import com.example.jobana.services.ReplyService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/replies")
class ReplyController(
    val replyService: ReplyService
) {
    @GetMapping("/my")
    fun getUserOffers() : ResponseEntity<List<ReplyEntity>> {
        return ResponseEntity(replyService.getUserReplies(), HttpStatus.OK)
    }
}