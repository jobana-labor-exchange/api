package com.example.jobana.exceptions

import org.springframework.http.HttpStatus

class BadRequestException(
    status: HttpStatus = HttpStatus.BAD_REQUEST,
    message : String = "Bad request"
) : AbstractApiException(
    status, message
)