package com.example.jobana.services

import com.example.jobana.entities.ContactEntity
import com.example.jobana.entities.UserEntity
import com.example.jobana.exceptions.ForbiddenException
import com.example.jobana.exceptions.ResourceNotFoundException
import com.example.jobana.models.request.ContactRequest
import com.example.jobana.models.response.ApiResponse
import com.example.jobana.models.response.DeletedResponse
import com.example.jobana.repositories.ContactsRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class ContactsService(
    val contactsRepository: ContactsRepository
) {
    fun getUserContacts(): List<ContactEntity> {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        return contactsRepository.getAllByUser(currentUser)
    }

    fun addContact(contactRequest: ContactRequest): ContactEntity {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        val newContact = ContactEntity(
            currentUser,
            contactRequest.key,
            contactRequest.value
        )

        return contactsRepository.save(newContact)
    }

    fun editContact(contactRequest: ContactRequest, id : UUID): ContactEntity {
        // try to get contact with this id
        val contact = getContactById(id)

        // save changes
        contact.key = contactRequest.key
        contact.value = contactRequest.value
        contactsRepository.save(contact)

        return contact
    }

    fun deleteContact( id : UUID) : ApiResponse {
        // try to get contact with this id
        val contact = getContactById(id)

        // save changes
        contactsRepository.delete(contact)

        return DeletedResponse(message = "Successfully deleted contact with id $id")
    }

    private fun getContactById(id : UUID) : ContactEntity{
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        // try to find contact with this id
        val contact = contactsRepository.findByIdOrNull(id)
            ?: throw ResourceNotFoundException(message = "Contact with id $id does not exist")

        // check if it's user contact
        if(contact.user.id != currentUser.id){
            throw ForbiddenException(message = "Current user has no access to contact with id $id")
        }

        return contact
    }
}