package com.example.jobana.models.request

data class ProfileRequest(
    val title: String,
    val shortDescription: String,
    val cv: String,
    val category : String,
    val city : String?
)