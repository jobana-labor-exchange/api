package com.example.jobana.repositories

import com.example.jobana.entities.PhotoEntity
import com.example.jobana.entities.UserEntity
import org.springframework.data.repository.CrudRepository
import java.util.UUID

interface PhotoRepository : CrudRepository<PhotoEntity, UUID>