package com.example.jobana.services

import com.example.jobana.entities.CityEntity
import com.example.jobana.repositories.CityRepository
import org.springframework.stereotype.Service

@Service
class CityService(
    val cityRepository: CityRepository
) {
    fun getAllCities(): List<CityEntity> {
        return cityRepository.getAll()
    }
}