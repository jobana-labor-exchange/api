package com.example.jobana.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.Entity
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name="offers")
class OfferEntity(
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "profile_id", nullable = false)
    var profile: ProfileEntity,

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "advert_id", nullable = false)
    // user who made the offer
    var user: UserEntity
) : GenericUUIDEntity()