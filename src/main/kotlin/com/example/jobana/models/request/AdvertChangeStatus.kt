package com.example.jobana.models.request

data class AdvertChangeStatus(
    val closed : Boolean
)