package com.example.jobana.models.request

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDate

data class RegisterRequest(
    val email: String,
    val password: String,
    val firstName: String,
    val lastName: String,
    @JsonFormat(pattern = "dd-MM-yyyy")
    val birthDate: LocalDate,
    val gender: String,
)