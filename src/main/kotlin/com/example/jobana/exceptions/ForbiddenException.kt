package com.example.jobana.exceptions

import org.springframework.http.HttpStatus

class ForbiddenException (
    status: HttpStatus = HttpStatus.FORBIDDEN,
    message : String = "The client does not have access rights to the content"
) : AbstractApiException(
    status, message
)