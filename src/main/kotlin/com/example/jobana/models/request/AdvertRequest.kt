package com.example.jobana.models.request


data class AdvertRequest(
    val title : String,
    val shortDescription : String,
    val description : String,
    val price : Int,
    val city : String?,
    val categories : List<String>
)