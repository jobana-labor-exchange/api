package com.example.jobana.controllers

import com.example.jobana.models.request.PatchUserRequest
import com.example.jobana.models.response.ApiResponse
import com.example.jobana.models.response.UserDataResponse
import com.example.jobana.services.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("/user")
class UserController(
    val userService: UserService,
) {

    @GetMapping
    fun getUserData(): ResponseEntity<UserDataResponse> {
        return ResponseEntity(userService.getUserData(), HttpStatus.OK)
    }

    @PatchMapping
    fun editUserData(@RequestBody patchUserRequest: PatchUserRequest): ResponseEntity<UserDataResponse> {
        return ResponseEntity(userService.editUserData(patchUserRequest), HttpStatus.OK)
    }


    @PostMapping("/img")
    fun uploadProfilePhoto(@RequestParam("image") multipartFile: MultipartFile): ResponseEntity<ApiResponse> {
        return userService.saveProfilePicture(multipartFile).asResponse()
    }

    @GetMapping("/img")
    fun getProfilePhoto(): ResponseEntity<ByteArray> {
        return userService.getProfilePicture()
    }


}