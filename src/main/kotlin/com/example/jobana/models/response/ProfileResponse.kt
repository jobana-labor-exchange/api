package com.example.jobana.models.response

import com.example.jobana.entities.*
import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDateTime
import java.util.*

data class ProfileResponse(
    val id : UUID,
    val userId: UUID,
    val title: String,
    val shortDescription: String,
    val cv: String,
    val city : String?,
    @get:JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    val createdAt : LocalDateTime,
    val category : String
){
    companion object{
        fun fromEntity(entity : ProfileEntity) =
            ProfileResponse(
                id = entity.id,
                userId = entity.user.id,
                title = entity.title,
                shortDescription = entity.shortDescription,
                cv = entity.cv,
                city = entity.city?.name,
                createdAt = entity.createdAt,
                category = entity.category.name
            )
    }
}