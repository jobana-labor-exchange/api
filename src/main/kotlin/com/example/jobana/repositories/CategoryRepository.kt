package com.example.jobana.repositories

import com.example.jobana.entities.CategoryEntity
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import java.util.UUID

interface CategoryRepository  : CrudRepository<CategoryEntity, UUID>{


    @Query("SELECT * FROM categories WHERE name='Default' LIMIT 1", nativeQuery = true)
    fun getDefaultCategory() : CategoryEntity

    fun getAllByParentCategoryId(id : UUID?) : List<CategoryEntity>
    fun getById(id : UUID?) : CategoryEntity

    fun getByName(name : String) : CategoryEntity?

    @Query("""WITH RECURSIVE subtree AS(
                SELECT c0.id, c0.created_at, c0.name, c0.short_description, c0.parent_category_id
                FROM categories c0 WHERE id = :id\:\:uuid
                UNION ALL
                SELECT c1.id, c1.created_at, c1.name, c1.short_description, c1.parent_category_id
                FROM categories c1 JOIN subtree s ON c1.parent_category_id = s.id
                )
        SELECT * FROM subtree;""", nativeQuery = true)
    fun getSubTreeById(@Param("id") id : String) : List<CategoryEntity>


}