package com.example.jobana.controllers

import com.example.jobana.models.response.ApiResponse
import com.example.jobana.services.AttachmentService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*


@RestController
@RequestMapping("/files")
class AttachmentController(
    val attachmentService: AttachmentService
) {

    @GetMapping("/{id}")
    fun downloadAttachment(@PathVariable id : UUID): ResponseEntity<ByteArray> {
        return attachmentService.downloadAttachment(id)
    }

    @DeleteMapping("/{id}")
    fun deleteAttachment(@PathVariable id : UUID) : ResponseEntity<ApiResponse>{
        return attachmentService.deleteAttachment(id).asResponse()
    }
}