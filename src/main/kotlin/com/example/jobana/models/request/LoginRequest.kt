package com.example.jobana.models.request


data class LoginRequest(
    val email: String,
    val password: String
)