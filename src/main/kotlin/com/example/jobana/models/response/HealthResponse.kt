package com.example.jobana.models.response

import org.springframework.http.HttpStatus
import java.time.LocalDateTime

class HealthResponse : ApiResponse {
    override val status = HttpStatus.OK
    override val message = "API is working"
    override val timestamp: LocalDateTime = LocalDateTime.now()

}