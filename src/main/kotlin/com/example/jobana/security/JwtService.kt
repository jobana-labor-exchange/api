package com.example.jobana.security

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.io.Decoders
import io.jsonwebtoken.security.Keys
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import java.security.Key
import java.util.*

@Component
class JwtService {

    private val secretKey = "26452948404D6251655468576D5A7134743777217A25432A462D4A614E645266"

    fun extractUsername(token: String) : String{
        val claims = extractClaims(token)
        return claims.subject
    }

    private fun extractClaims(token : String) : Claims{
        return Jwts
            .parserBuilder()
            .setSigningKey(getSingingKey())
            .build()
            .parseClaimsJws(token)
            .body
    }

    fun generateToken(userDetails: UserDetails, extraClaims : Map<String, Any>?  = null): String {
        return Jwts
            .builder()
            .setClaims(extraClaims?: mapOf<String, Any>())
            .setSubject(userDetails.username)
            .setIssuedAt(Date(System.currentTimeMillis()))
            .setExpiration(Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 14))
            .signWith(getSingingKey(), SignatureAlgorithm.HS256)
            .compact()
    }

    fun isTokenValid(userDetails: UserDetails, token : String) : Boolean{
        return (userDetails.username == extractUsername(token)) && !isTokenExpired(token)
    }

    private fun isTokenExpired(token : String): Boolean{
        val expDate = extractClaims(token).expiration
        return expDate.before(Date(System.currentTimeMillis()))
    }

    private fun getSingingKey(): Key {
        val keyBytes = Decoders.BASE64.decode(secretKey)
        return Keys.hmacShaKeyFor(keyBytes)

    }
}