package com.example.jobana.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*


@Entity
@Table(name = "attachments")
class AttachmentEntity(

    @Column(nullable = false)
    var filePath: String,

    @Column(nullable = false)
    var contentType: String,

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "advert_id", nullable = false)
    var advert: AdvertEntity,
) : GenericUUIDEntity()