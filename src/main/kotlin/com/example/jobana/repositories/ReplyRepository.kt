package com.example.jobana.repositories

import com.example.jobana.entities.*
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ReplyRepository : CrudRepository<ReplyEntity, UUID>{
    fun findByAdvertIdAndUserId(profileId : UUID, userId: UUID) : ReplyEntity?

    fun findAllByAdvertAuthor(user : UserEntity) : List<ReplyEntity>

    fun findAllByAdvert(advertEntity: AdvertEntity) : List<ReplyEntity>
}