package com.example.jobana.repositories

import com.example.jobana.entities.AdvertEntity
import com.example.jobana.entities.UserEntity
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface AdvertRepository : CrudRepository<AdvertEntity, UUID>, PagingAndSortingRepository<AdvertEntity, UUID>{
    fun findAllByAuthor(author : UserEntity) : List<AdvertEntity>

    @Query("""SELECT DISTINCT a.id, a.created_at, a.closed_at, a.description, price,
       a.short_description, a.title, a.updated_at, a.author_id, a.city_id FROM adverts a
    INNER JOIN cities ci ON a.city_id = ci.id
            WHERE ci.name = :city ORDER BY a.created_at DESC""", nativeQuery = true)
    fun findAllByCityPaged(city : String, pageable: Pageable) : Page<AdvertEntity>

    @Query("""SELECT DISTINCT a.id, a.created_at, a.closed_at, a.description, price,
       a.short_description, a.title, a.updated_at, a.author_id, a.city_id FROM adverts a
    INNER JOIN adverts_categories ac on a.id = ac.adverts_id
    INNER JOIN categories c ON ac.categories_id = c.id
    INNER JOIN cities ci ON a.city_id = ci.id
            WHERE c.name IN :categories AND ci.name = :city ORDER BY a.created_at DESC""", nativeQuery = true)
    fun findAllByCategoriesAndCityPaged(categories: List<String>, city: String, pageable: Pageable) : Page<AdvertEntity>

    @Query("""SELECT DISTINCT a.id, a.created_at, a.closed_at, a.description, price,
       a.short_description, a.title, a.updated_at, a.author_id, a.city_id FROM adverts a
    INNER JOIN adverts_categories ac on a.id = ac.adverts_id
    INNER JOIN categories c ON ac.categories_id = c.id WHERE c.name IN :categories ORDER BY a.created_at DESC""", nativeQuery = true)
    fun findAllByCategoriesPaged(categories: List<String>, pageable: Pageable) : Page<AdvertEntity>

}