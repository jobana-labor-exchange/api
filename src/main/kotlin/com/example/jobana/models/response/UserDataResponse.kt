package com.example.jobana.models.response


import com.example.jobana.entities.UserEntity
import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDate

data class UserDataResponse(
    val email: String,
    val firstName: String,
    val lastName: String,
    @JsonFormat(pattern = "dd-MM-yyyy")
    val birthDate: LocalDate,
    var gender: String
){
    companion object{
        fun fromEntity(userEntity: UserEntity): UserDataResponse {
            return UserDataResponse(
                email = userEntity.email,
                firstName = userEntity.firstName,
                lastName = userEntity.lastName,
                birthDate = userEntity.birthDate,
                gender = userEntity.gender.name
            )
        }
    }
}