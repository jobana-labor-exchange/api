package com.example.jobana.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.OneToMany
import jakarta.persistence.Table

@Entity
@Table(name = "cities")
class CityEntity(

    @Column(nullable = false, unique = true, length = 64)
    var name: String,

    @JsonIgnore
    @OneToMany(mappedBy = "city")
    // adverts posted at this city
    var adverts: List<AdvertEntity>,

    @JsonIgnore
    @OneToMany(mappedBy = "city")
    // profiles posted at this city
    var profiles: List<ProfileEntity>

) : GenericUUIDEntity()