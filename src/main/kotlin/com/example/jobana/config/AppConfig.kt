package com.example.jobana.config

import com.example.jobana.security.UserDetailsServiceImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder

@Configuration

class AppConfig (
    val userDetailsService: UserDetailsServiceImpl

){
    @Bean
    fun authManager (config : AuthenticationConfiguration): AuthenticationManager {
        return config.authenticationManager
    }

    @Bean
    fun authProvider() : AuthenticationProvider {
        val daoProvider = DaoAuthenticationProvider()
        daoProvider.setUserDetailsService(userDetailsService)
        daoProvider.setPasswordEncoder(passwordEncoder())
        return daoProvider
    }

    @Bean
    fun passwordEncoder() : PasswordEncoder = BCryptPasswordEncoder()
}