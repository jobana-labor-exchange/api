package com.example.jobana.models.response

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.time.LocalDateTime

interface ApiResponse {
    val status: HttpStatus
    val message: String
    @get:JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss", timezone = "Europe/Moscow")
    val timestamp: LocalDateTime

    fun asResponse() = ResponseEntity.status(status).body(this)
}
