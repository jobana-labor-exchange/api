package com.example.jobana.controllers

import com.example.jobana.models.request.LoginRequest
import com.example.jobana.models.request.RegisterRequest
import com.example.jobana.models.response.ApiResponse
import com.example.jobana.services.UserService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/auth")
class AuthController(
    val userService: UserService
) {
    @PostMapping("/signup")
    fun createUser(@RequestBody registerRequest: RegisterRequest): ResponseEntity<ApiResponse> {
        return userService.registerUser(registerRequest).asResponse()
    }

    @PostMapping("/login")
    fun loginUser(@RequestBody loginRequest: LoginRequest): ResponseEntity<ApiResponse> {
        return userService.authenticateUser(loginRequest).asResponse()
    }
}