package com.example.jobana.entities

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import jakarta.persistence.*


@Entity
@Table(name="categories")
@JsonIgnoreProperties("createdAt")
@JsonPropertyOrder("id", "name", "children")
class CategoryEntity(
    @Column(nullable = false, length = 64)
    var name: String,

    @JsonIgnore // TODO remove
    @Column(length = 128)
    var shortDescription: String,

    @JsonIgnore
    @ManyToMany(mappedBy = "categories")
    val adverts : List<AdvertEntity>,

    @JsonIgnore
    @OneToMany(mappedBy = "category")
    val profiles : List<ProfileEntity>,

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "parent_category_id")
    var parentCategory: CategoryEntity?,

    @OneToMany(mappedBy = "parentCategory")
    @JsonAlias("children")
    var childCategories: List<CategoryEntity>,

    ) : GenericUUIDEntity()