package com.example.jobana.services

import com.example.jobana.entities.CategoryEntity
import com.example.jobana.repositories.CategoryRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class CategoryService(
    val categoryRepository: CategoryRepository
) {
    fun getCategoryTree() : List<CategoryEntity>{
        return categoryRepository.getAllByParentCategoryId(null)
    }

    fun getCategorySubTree(id : UUID) : CategoryEntity{
        return categoryRepository.getById(id)
    }
}