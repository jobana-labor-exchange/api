package com.example.jobana.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*

@Entity
@Table(name="contacts")
class ContactEntity(
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    var user: UserEntity,

    @Column(name="contact_name", nullable = false, length = 32)
    var key: String,

    @Column(nullable = false, length = 128)
    var value: String,

    ) : GenericUUIDEntity()