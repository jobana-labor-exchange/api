package com.example.jobana.controllers

import com.example.jobana.entities.CityEntity
import com.example.jobana.services.CityService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/cities")
class CitiesController(
    val cityService: CityService
) {
    @GetMapping
    fun getAllCities() : ResponseEntity<List<CityEntity>>{
        return ResponseEntity(cityService.getAllCities(), HttpStatus.OK)
    }
}