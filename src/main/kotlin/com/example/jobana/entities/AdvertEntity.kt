package com.example.jobana.entities

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*
import java.time.LocalDateTime

@Entity
@Table(name = "adverts")
class AdvertEntity(
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "author_id", nullable = false)
    var author: UserEntity,

    @Column(nullable = false, length = 64)
    var title: String,

    @Column(length = 128)
    var shortDescription: String,

    @Column(nullable = false, columnDefinition = "TEXT")
    var description: String,

    @Column
    @get:JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss", timezone = "Europe/Moscow")
    var updatedAt: LocalDateTime,

    @Column
    @get:JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss", timezone = "Europe/Moscow")
    var closedAt: LocalDateTime?,

    @OneToMany(mappedBy="advert")
    val attachments : List<AttachmentEntity>,

    @Column(nullable = false)
    var price: Int,

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "city_id")
    var city: CityEntity?,

    @JsonIgnore
    @OneToMany(mappedBy = "advert")
    var replies: List<ReplyEntity>,

    @JsonIgnore
    @ManyToMany
    val categories : List<CategoryEntity>

) : GenericUUIDEntity()

