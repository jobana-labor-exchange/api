package com.example.jobana.controllers

import com.example.jobana.entities.OfferEntity
import com.example.jobana.services.OfferService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/offers")
class OfferController(
    val offerService: OfferService
) {
    @GetMapping("/my")
    fun getUserOffers() : ResponseEntity<List<OfferEntity>> {
        return ResponseEntity(offerService.getUserOffers(), HttpStatus.OK)
    }
}