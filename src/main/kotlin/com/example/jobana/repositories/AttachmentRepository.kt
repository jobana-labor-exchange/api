package com.example.jobana.repositories

import com.example.jobana.entities.AttachmentEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface AttachmentRepository : CrudRepository<AttachmentEntity, UUID>