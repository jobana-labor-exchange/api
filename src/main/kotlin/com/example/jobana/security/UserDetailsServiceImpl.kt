package com.example.jobana.security

import com.example.jobana.exceptions.ResourceNotFoundException
import com.example.jobana.repositories.UserRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Component

@Component
class UserDetailsServiceImpl(
    val userDao: UserRepository
) : UserDetailsService {
    // to avoid cycle dependencies
    override fun loadUserByUsername(email: String): UserDetails {
        return userDao.findByEmail(email)
            ?: throw ResourceNotFoundException(message = "User with email '$email' not found")
    }
}