CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- insert cities if necessary
INSERT INTO cities (id, name, created_at)
SELECT uuid_generate_v4(), 'Москва', NOW()
WHERE NOT EXISTS(SELECT * FROM cities WHERE name='Москва');

INSERT INTO cities (id, name, created_at)
SELECT uuid_generate_v4(), 'Бишкек', NOW()
WHERE NOT EXISTS(SELECT * FROM cities WHERE name='Бишкек');

INSERT INTO cities (id, name, created_at)
SELECT uuid_generate_v4(), 'Самара', NOW()
WHERE NOT EXISTS(SELECT * FROM cities WHERE name='Самара');

INSERT INTO cities (id, name, created_at)
SELECT uuid_generate_v4(), 'Тюмень', NOW()
WHERE NOT EXISTS(SELECT * FROM cities WHERE name='Тюмень');

-- insert categories
INSERT INTO categories (id, created_at, name, short_description, parent_category_id)
SELECT uuid_generate_v4(), NOW(), 'Репетиторы', '', null
WHERE NOT EXISTS(SELECT * FROM categories WHERE name='Репетиторы');

INSERT INTO categories (id, created_at, name, short_description, parent_category_id)
SELECT uuid_generate_v4(), NOW(), 'Английский язык', '', (SELECT id FROM categories WHERE name='Репетиторы' LIMIT 1)
WHERE NOT EXISTS(SELECT * FROM categories WHERE name='Английский язык');

INSERT INTO categories (id, created_at, name, short_description, parent_category_id)
SELECT uuid_generate_v4(), NOW(), 'Русский язык', '', (SELECT id FROM categories WHERE name='Репетиторы' LIMIT 1)
WHERE NOT EXISTS(SELECT * FROM categories WHERE name='Русский язык');

INSERT INTO categories (id, created_at, name, short_description, parent_category_id)
SELECT uuid_generate_v4(), NOW(), 'Математика', '', (SELECT id FROM categories WHERE name='Репетиторы' LIMIT 1)
WHERE NOT EXISTS(SELECT * FROM categories WHERE name='Математика');

INSERT INTO categories (id, created_at, name, short_description, parent_category_id)
SELECT uuid_generate_v4(), NOW(), 'Математика ОГЭ', '', (SELECT id FROM categories WHERE name='Математика' LIMIT 1)
WHERE NOT EXISTS(SELECT * FROM categories WHERE name='Математика ОГЭ');

INSERT INTO categories (id, created_at, name, short_description, parent_category_id)
SELECT uuid_generate_v4(), NOW(), 'Математика ЕГЭ', '', (SELECT id FROM categories WHERE name='Математика' LIMIT 1)
WHERE NOT EXISTS(SELECT * FROM categories WHERE name='Математика ЕГЭ');

INSERT INTO categories (id, created_at, name, short_description, parent_category_id)
SELECT uuid_generate_v4(), NOW(), 'Ремонт', '', null
WHERE NOT EXISTS(SELECT * FROM categories WHERE name='Ремонт');

INSERT INTO categories (id, created_at, name, short_description, parent_category_id)
SELECT uuid_generate_v4(), NOW(), 'Сантехники', '', (SELECT id FROM categories WHERE name='Ремонт' LIMIT 1)
WHERE NOT EXISTS(SELECT * FROM categories WHERE name='Сантехники');

INSERT INTO categories (id, created_at, name, short_description, parent_category_id)
SELECT uuid_generate_v4(), NOW(), 'Электрики', '', (SELECT id FROM categories WHERE name='Ремонт' LIMIT 1)
WHERE NOT EXISTS(SELECT * FROM categories WHERE name='Электрики');

INSERT INTO categories (id, created_at, name, short_description, parent_category_id)
SELECT uuid_generate_v4(), NOW(), 'Плиточники', '', (SELECT id FROM categories WHERE name='Ремонт' LIMIT 1)
WHERE NOT EXISTS(SELECT * FROM categories WHERE name='Плиточники');