package com.example.jobana.models.response

import org.springframework.http.HttpStatus
import java.time.LocalDateTime

class AuthResponse(
    val token :  String?,
    override val status: HttpStatus,
    override val message: String,
    override val timestamp: LocalDateTime = LocalDateTime.now()
) : ApiResponse