package com.example.jobana.entities

import com.fasterxml.jackson.annotation.JsonFormat
import jakarta.persistence.Column
import jakarta.persistence.Id
import jakarta.persistence.MappedSuperclass
import java.io.Serializable
import java.time.LocalDateTime
import java.util.UUID


@MappedSuperclass
abstract class GenericUUIDEntity : Serializable {

    @Id
    var id: UUID = UUID.randomUUID()

    @Column(nullable = false, updatable = false)
    @get:JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss", timezone = "Europe/Moscow")
    val createdAt: LocalDateTime = LocalDateTime.now()

}