package com.example.jobana.services

import com.example.jobana.exceptions.InvalidRequestDataException
import com.example.jobana.models.request.AdvertRequest
import com.example.jobana.entities.AdvertEntity
import com.example.jobana.entities.UserEntity
import com.example.jobana.exceptions.ForbiddenException
import com.example.jobana.exceptions.ResourceNotFoundException
import com.example.jobana.models.request.AdvertChangeStatus
import com.example.jobana.models.response.AdvertResponse
import com.example.jobana.models.response.ApiResponse
import com.example.jobana.models.response.DeletedResponse
import com.example.jobana.repositories.AdvertRepository
import com.example.jobana.repositories.CategoryRepository
import com.example.jobana.repositories.CityRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.*


@Service
class AdvertService(
    val advertRepository: AdvertRepository,
    val cityRepository: CityRepository,
    val categoryRepository: CategoryRepository
) {

    fun getAdvertById(id: UUID): AdvertEntity {
        return advertRepository.findByIdOrNull(id)
            ?: throw ResourceNotFoundException(message = "Advert with id $id does not exist")
    }

    fun searchPaged(page: Int, size: Int, cityName: String?, categoryNames: List<String>?): Page<AdvertResponse> {

        val pageable = PageRequest.of(page, size)

        // check city
        val city = cityName?.let {
            cityRepository.getByName(it)
                ?: throw InvalidRequestDataException(message = "City with name '$cityName' is not found")
        }

        // check categories
        val categories = categoryNames?.flatMap { categoryName ->
            val category = categoryRepository.getByName(categoryName)
                ?: throw InvalidRequestDataException(message = "Category with name '$categoryName' is not found")
            categoryRepository.getSubTreeById(category.id.toString())
        }?.distinct()

        // search
        val searchResult = if (city != null && categories != null) {
            advertRepository.findAllByCategoriesAndCityPaged(categories.map { it.name }, city.name, pageable)
        } else if (city != null) {
            advertRepository.findAllByCityPaged(city.name, pageable)
        } else if (categories != null) {
            advertRepository.findAllByCategoriesPaged(categories.map { it.name }, pageable)
        } else {
            advertRepository.findAll(pageable)
        }

        return searchResult.map { AdvertResponse.fromEntity(it) }

    }

    fun createAdvert(advertRequest: AdvertRequest): AdvertEntity {
        // check city
        val city = advertRequest.city?.let {
            cityRepository.getByName(it)
                ?: throw InvalidRequestDataException(message = "City with name '${advertRequest.city}' is not found")
        }

        // check categories
        val categories = advertRequest.categories.map {
            categoryRepository.getByName(it)
                ?: throw InvalidRequestDataException(message = "Category with name '$it' is not found")
        }

        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        val newAdvert = AdvertEntity(
            currentUser,
            advertRequest.title,
            advertRequest.shortDescription,
            advertRequest.description,
            LocalDateTime.now(),
            null,
            listOf(),
            advertRequest.price,
            city,
            listOf(),
            categories
        )

        return advertRepository.save(newAdvert)

    }

    fun getUserAdverts(): List<AdvertResponse> {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        return advertRepository.findAllByAuthor(currentUser).map { AdvertResponse.fromEntity(it) }
    }

    fun editAdvert(advertRequest: AdvertRequest, id: UUID): AdvertResponse {
        // try to find advert with this id
        val advert = getMyAdvertById(id)

        // check city
        val city = advertRequest.city?.let {
            cityRepository.getByName(it)
                ?: throw InvalidRequestDataException(message = "City with name '${advertRequest.city}' is not found")
        }

        // save changes
        advert.title = advertRequest.title
        advert.shortDescription = advertRequest.shortDescription
        advert.description = advertRequest.description
        advert.price = advertRequest.price
        advert.city = city
        advert.updatedAt = LocalDateTime.now()

        advertRepository.save(advert)

        return AdvertResponse.fromEntity(advert)
    }

    fun deleteAdvert(id: UUID): ApiResponse {
        // try to find advert with this id
        val advert = getMyAdvertById(id)

        // save changes
        advertRepository.delete(advert)

        return DeletedResponse(message = "Successfully deleted advert with id $id")
    }

    fun changeAdvertStatus(advertChangeStatus: AdvertChangeStatus, id: UUID): ApiResponse {
        // try to find advert with this id
        val advert = getMyAdvertById(id)

        val message = if (advertChangeStatus.closed) {
            if (advert.closedAt == null) {
                advert.closedAt = LocalDateTime.now()
                "Advert with id $id was closed successfully"
            } else {
                // do nothing if advert is already closed
                "Advert with id $id has already been closed at ${advert.closedAt}"
            }
        } else {
            if (advert.closedAt == null) {
                "Advert with id $id was not closed"
            } else {
                advert.closedAt = null
                "Advert with id $id is not closed now"
            }
        }

        // save changes
        advertRepository.save(advert)

        return object : ApiResponse {
            override val message = message
            override val status = HttpStatus.OK
            override val timestamp = LocalDateTime.now()
        }
    }

    private fun getMyAdvertById(id: UUID): AdvertEntity {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        // try to find advert with this id
        val advert = advertRepository.findByIdOrNull(id)
            ?: throw ResourceNotFoundException(message = "Advert with id $id does not exist")

        // check if it's user advert
        if (advert.author.id != currentUser.id) {
            throw ForbiddenException(message = "Current user has no edit access to advert with id $id")
        }

        return advert
    }


}