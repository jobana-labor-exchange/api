package com.example.jobana.services

import com.example.jobana.entities.ProfileEntity
import com.example.jobana.entities.UserEntity
import com.example.jobana.exceptions.ForbiddenException
import com.example.jobana.exceptions.InvalidRequestDataException
import com.example.jobana.exceptions.ResourceNotFoundException
import com.example.jobana.models.request.ProfileRequest
import com.example.jobana.models.response.ApiResponse
import com.example.jobana.models.response.DeletedResponse
import com.example.jobana.models.response.ProfileResponse
import com.example.jobana.repositories.CategoryRepository
import com.example.jobana.repositories.CityRepository
import com.example.jobana.repositories.ProfilesRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.util.*

@Service
class ProfilesService(
    val profilesRepository: ProfilesRepository,
    val categoryRepository: CategoryRepository,
    val cityRepository: CityRepository
) {


    fun getProfileById(id: UUID): ProfileEntity {
        return profilesRepository.findByIdOrNull(id)
            ?: throw ResourceNotFoundException(message = "Profile with id $id does not exist")
    }

    fun searchPaged(page: Int, size: Int, cityName: String?, categoryNames: List<String>?): Page<ProfileResponse> {

        val pageable = PageRequest.of(page, size)

        // check city
        val city = cityName?.let {
            cityRepository.getByName(it)
                ?: throw InvalidRequestDataException(message = "City with name '$cityName' is not found")
        }

        // check categories
        val categories = categoryNames?.flatMap { categoryName ->
            val category = categoryRepository.getByName(categoryName)
                ?: throw InvalidRequestDataException(message = "Category with name '$categoryName' is not found")
            categoryRepository.getSubTreeById(category.id.toString())
        }?.distinct()

        // search
        val searchResult = if (city != null && categories != null) {
            profilesRepository.findAllByCategoriesAndCityPaged(categories.map{it.name}, city.name, pageable)
        } else if (city != null) {
            profilesRepository.findAllByCityPaged(city.name, pageable)
        } else if (categories != null) {
            profilesRepository.findAllByCategoriesPaged(categories.map{it.name}, pageable)
        } else {
            profilesRepository.findAll(pageable)
        }

        return searchResult.map { ProfileResponse.fromEntity(it) }

    }

    fun getUserProfiles(): List<ProfileResponse> {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        return profilesRepository.findAllByUser(currentUser).map { ProfileResponse.fromEntity(it) }
    }

    fun createProfile(profileRequest: ProfileRequest): ProfileResponse {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        // check city
        val city = profileRequest.city?.let {
            cityRepository.getByName(it)
                ?: throw InvalidRequestDataException(message = "City with name '${profileRequest.city}' is not found")
        }

        // check category
        val category = categoryRepository.getByName(profileRequest.category)
            ?: throw InvalidRequestDataException(message = "Category with name '${profileRequest.category}' is not found")

        val newProfile = ProfileEntity(
            user = currentUser,
            title = profileRequest.title,
            shortDescription = profileRequest.shortDescription,
            cv = profileRequest.cv,
            category = category,
            city = city
        )

        return ProfileResponse.fromEntity(profilesRepository.save(newProfile))
    }

    fun editProfile(profileRequest: ProfileRequest, id: UUID): ProfileResponse {
        // try to get profile with this id
        val profile = getMyProfileById(id)

        // check city
        val city = profileRequest.city?.let {
            cityRepository.getByName(it)
                ?: throw InvalidRequestDataException(message = "City with name '${profileRequest.city}' is not found")
        }

        // check category
        val category = categoryRepository.getByName(profileRequest.category)
            ?: throw InvalidRequestDataException(message = "Category with name '${profileRequest.category}' is not found")

        profile.title = profileRequest.title
        profile.shortDescription = profileRequest.shortDescription
        profile.cv = profileRequest.cv
        profile.category = category
        profile.city = city

        profilesRepository.save(profile)

        return ProfileResponse.fromEntity(profile)
    }

    fun deleteProfile(id: UUID): ApiResponse {
        // try to get profile with this id
        val profile = getMyProfileById(id)

        profilesRepository.delete(profile)

        return DeletedResponse(message = "Successfully deleted profile with id $id")
    }

    private fun getMyProfileById(id: UUID): ProfileEntity {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        // try to find profile with this id
        val profile = profilesRepository.findByIdOrNull(id)
            ?: throw ResourceNotFoundException(message = "Profile with id $id does not exist")

        // check if it's user advert
        if (profile.user.id != currentUser.id) {
            throw ForbiddenException(message = "Current user has no edit access to profile with id $id")
        }

        return profile
    }

}