package com.example.jobana.services

import com.example.jobana.entities.OfferEntity
import com.example.jobana.entities.UserEntity
import com.example.jobana.exceptions.ForbiddenException
import com.example.jobana.exceptions.InvalidRequestDataException
import com.example.jobana.models.response.ApiResponse
import com.example.jobana.repositories.OfferRepository
import com.example.jobana.repositories.ProfilesRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.*

@Service
class OfferService(
    val offerRepository: OfferRepository,
    val profilesRepository: ProfilesRepository
) {

    fun addOffer(profileId: UUID): OfferEntity {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        // check profile
        val profile = profilesRepository.findByIdOrNull(profileId)
            ?: throw InvalidRequestDataException(message = "Profile with id $profileId is not found")

        // check that offer has not been made yet
        val offer = offerRepository.findByProfileIdAndUserId(profileId, currentUser.id)
        if (offer == null) {
            val newOffer = OfferEntity(profile, currentUser)
            offerRepository.save(newOffer)
            return newOffer
        }
        return offer

    }

    fun cancelOffer(profileId: UUID): ApiResponse {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        // check profile
        val profile = profilesRepository.findByIdOrNull(profileId)
            ?: throw InvalidRequestDataException(message = "Profile with id $profileId is not found")

        // check that offer has been made
        val offer = offerRepository.findByProfileIdAndUserId(profileId, currentUser.id)

        val responseMessage =
            if (offer == null) {
                "Offer has not been made to profile $profileId from current user "

            } else {
                offerRepository.delete(offer)
                "Offer cancelled successfully"
            }

        return object : ApiResponse {
            override val message = responseMessage
            override val status = HttpStatus.OK
            override val timestamp = LocalDateTime.now()
        }
    }


    fun getUserOffers(): List<OfferEntity> {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity
        return offerRepository.findAllByProfileUser(currentUser)
    }

    fun getProfileOffers(profileId: UUID): List<OfferEntity> {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        // check profile
        val profile = profilesRepository.findByIdOrNull(profileId)
            ?: throw InvalidRequestDataException(message = "Profile with id $profileId is not found")

        if (profile.user.id != currentUser.id) {
            throw ForbiddenException(message = "Current user has no access to offers of profile with id $profileId")
        }

        return offerRepository.findAllByProfile(profile)

    }

}