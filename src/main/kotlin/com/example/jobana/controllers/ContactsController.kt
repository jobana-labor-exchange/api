package com.example.jobana.controllers

import com.example.jobana.entities.ContactEntity
import com.example.jobana.models.request.ContactRequest
import com.example.jobana.models.response.ApiResponse
import com.example.jobana.services.ContactsService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/contacts")
class ContactsController(
    val contactsService: ContactsService

) {

    @GetMapping("/my")
    fun getUserContacts() : ResponseEntity<List<ContactEntity>> {
        return ResponseEntity(contactsService.getUserContacts(), HttpStatus.OK)
    }

    @PostMapping
    fun addContact(@RequestBody contactRequest: ContactRequest) : ResponseEntity<ContactEntity> {
        return ResponseEntity(contactsService.addContact(contactRequest), HttpStatus.CREATED)
    }

    @PutMapping("/{id}")
    fun editContact(@RequestBody contactRequest: ContactRequest, @PathVariable id : UUID) : ResponseEntity<ContactEntity> {
        return ResponseEntity(contactsService.editContact(contactRequest, id), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    fun deleteContact(@PathVariable id : UUID) : ResponseEntity<ApiResponse> {
        return contactsService.deleteContact(id).asResponse()
    }
}