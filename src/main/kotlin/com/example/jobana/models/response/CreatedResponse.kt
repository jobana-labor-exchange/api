package com.example.jobana.models.response

import org.springframework.http.HttpStatus
import java.time.LocalDateTime

class CreatedResponse(
    override val message : String,
    val createdEntity : Any
) : ApiResponse {
    override val status = HttpStatus.CREATED
    override val timestamp: LocalDateTime = LocalDateTime.now()
}