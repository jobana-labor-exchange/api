package com.example.jobana.controllers

import com.example.jobana.entities.CategoryEntity
import com.example.jobana.services.CategoryService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*


@RestController
@RequestMapping("/categories")
class CategoryController(
    val categoryService: CategoryService
) {
    @GetMapping
    fun getCategorySubTree(@RequestParam id : UUID): CategoryEntity {
        return categoryService.getCategorySubTree(id)
    }

    @GetMapping("/all")
    fun getCategoryTree() : List<CategoryEntity>{
        return categoryService.getCategoryTree()
    }
}