package com.example.jobana.services

import com.example.jobana.entities.ReplyEntity
import com.example.jobana.entities.UserEntity
import com.example.jobana.exceptions.ForbiddenException
import com.example.jobana.exceptions.InvalidRequestDataException
import com.example.jobana.models.response.ApiResponse
import com.example.jobana.repositories.AdvertRepository
import com.example.jobana.repositories.ReplyRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.*

@Service
class ReplyService(
    val replyRepository: ReplyRepository,
    val advertRepository: AdvertRepository
) {

    fun addReply(advertId: UUID): ReplyEntity {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        // check advert
        val advert = advertRepository.findByIdOrNull(advertId)
            ?: throw InvalidRequestDataException(message = "Advert with id $advertId is not found")

        // check that offer has not been made yet
        val reply = replyRepository.findByAdvertIdAndUserId(advertId, currentUser.id)
        if (reply == null) {
            val newReply = ReplyEntity(advert, currentUser)
            replyRepository.save(newReply)
            return newReply
        }
        return reply

    }

    fun cancelReply(advertId: UUID): ApiResponse {
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        // check advert
        val advert = advertRepository.findByIdOrNull(advertId)
            ?: throw InvalidRequestDataException(message = "Advert with id $advertId is not found")

        // check that reply has been made
        val reply = replyRepository.findByAdvertIdAndUserId(advertId, currentUser.id)

        val responseMessage =
            if (reply == null) {
                "Reply has not been made to advert $advertId from current user "

            } else {
                replyRepository.delete(reply)
                "Reply cancelled successfully"
            }

        return object : ApiResponse {
            override val message = responseMessage
            override val status = HttpStatus.OK
            override val timestamp = LocalDateTime.now()
        }
    }

    fun getUserReplies() : List<ReplyEntity>{
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity
        return replyRepository.findAllByAdvertAuthor(currentUser)
    }

    fun getAdvertReplies(advertId: UUID) : List<ReplyEntity>{
        val currentUser = SecurityContextHolder.getContext().authentication.principal as UserEntity

        // check advert
        val advert = advertRepository.findByIdOrNull(advertId)
            ?: throw InvalidRequestDataException(message = "Advert with id $advertId is not found")

        if(advert.author.id != currentUser.id){
            throw ForbiddenException(message = "Current user has no access to replies of advert with id $advertId")
        }

        return replyRepository.findAllByAdvert(advert)

    }

}